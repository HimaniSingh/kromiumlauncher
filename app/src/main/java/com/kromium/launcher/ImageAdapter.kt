package com.kromium.launcher

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.util.*

internal class ImageAdapter(private val mContext: Context, private val Imageid: IntArray) :
    BaseAdapter() {
    private val imageUrls = arrayOf(
        "http://www.jarpy.in/chromium/wall1.jpg",
        "http://www.jarpy.in/chromium/wall2.jpg",
        "http://www.jarpy.in/chromium/wall3.jpg",
        "http://www.jarpy.in/chromium/wall4.jpg",
        "http://www.jarpy.in/chromium/wall5.jpg",
        "http://www.jarpy.in/chromium/wall6.jpg"
    )
    private var error = false
    var errorMessage: String? = ""
        private set

    override fun getCount(): Int {
        return Imageid.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    @SuppressLint("InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val gridView: View
        if (convertView == null) {
            /*   gridView = new View(mContext);
             */
            gridView = Objects.requireNonNull(inflater).inflate(R.layout.list_item_wallpaper, null)
            val imageView = gridView.findViewById<ImageView>(R.id.item_wall_thumb)
            imageView.setImageResource(Imageid[position])
            val picasso = Picasso.Builder(parent.context)
                .listener { picasso1: Picasso?, uri: Uri?, exception: Exception ->
                    error = true
                    errorMessage = exception.message
                    exception.printStackTrace()
                }
                .build()
            picasso.load(imageUrls[position])
                .error(R.drawable.error)
                .into(imageView)
        } else {
            gridView = convertView
        }
        return gridView
    }

    val isLoaded: Boolean
        get() = !error
}