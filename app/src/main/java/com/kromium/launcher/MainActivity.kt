package com.kromium.launcher

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.andrognito.flashbar.Flashbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_main.*
import smartdevelop.ir.eram.showcaseviewlib.GuideView
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType


@Suppress("IMPLICIT_BOXING_IN_IDENTITY_EQUALS")
class MainActivity : AppCompatActivity(), DrawerAdapter.AppDrawerListener {
    private lateinit var flash: Flashbar
    private lateinit var adapter: DockAdapter
    var flag2row = false
    private lateinit var viewPager: ViewPager2

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        Dexter.withContext(this)
            .withPermissions(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) { /* ... */
                    }

                    override fun onPermissionRationaleShouldBeShown(
                            permissions: List<PermissionRequest?>?,
                            token: PermissionToken?
                    ) { /* ... */
                    }
                }).check()
        val isDefault: Boolean = Utilities().isMyLauncherDefault(this)
        if (!isDefault) {
            flash = Flashbar.Builder(this)
                .gravity(Flashbar.Gravity.BOTTOM)
                .title(R.string.app_name)
                .message("Chromium launcher has not been set as your default launcher")
                    .messageSizeInSp(12.0f)
                .duration(3000)
                .primaryActionText("Set as default")
                    .primaryActionTextColor(Color.BLACK)
                    .primaryActionTextTypeface(Typeface.DEFAULT_BOLD)
                .primaryActionTapListener(object : Flashbar.OnActionTapListener {
                    override fun onActionTapped(bar: Flashbar) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            val intent = Intent(Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        }
                    }
                })
                .backgroundColorRes(R.color.default_color)
                .build()
            flash.show()
        }
    }


    override fun onBackPressed() {
        home_viewpager.setCurrentItem(0, true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager = findViewById(R.id.home_viewpager)
        viewPager.reduceDragSensitivity()
        viewPager.adapter = ViewPagerAdapter(this)
        viewPager.setPageTransformer(ZoomOutTransformation())
        windowArgs()
        addClickListeners()
        prepareDockApps()
    }


    private fun addClickListeners() {
        @Suppress("DEPRECATION")
        all_apps_btn.setOnClickListener {
            home_viewpager.setCurrentItem(1, true)
        }
    }

    private fun windowArgs() {
        window.apply {
            @Suppress("DEPRECATION")
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            @Suppress("DEPRECATION")
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                statusBarColor = Color.TRANSPARENT
            }
        }
    }


    fun shakeItems(context: Context, view: View) {
        val animShake = AnimationUtils.loadAnimation(context, R.anim.shake)
        view.startAnimation(animShake)
    }


    @SuppressLint("ObsoleteSdkInt")
    @Suppress("DEPRECATION")
    fun FullScreencall() {
        if (Build.VERSION.SDK_INT in 12..18) { // lower api
            val v: View = this.window.decorView
            v.systemUiVisibility = View.GONE
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            val decorView: View = window.decorView
            val uiOptions =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            decorView.systemUiVisibility = uiOptions
        }
    }

    private fun prepareDockApps() {
        var navigationBarHeight = 0
        val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        if (resourceId > 0) {
            navigationBarHeight = resources.getDimensionPixelSize(resourceId)
        }
        if (navigationBarHeight > 0) {
            val param = dock_layout.layoutParams as RelativeLayout.LayoutParams
            param.bottomMargin = navigationBarHeight / 4
            dock_layout.layoutParams = param
        }
        dockEditBtn.setOnClickListener {
            shakeItems(this, recyclerViewDock)
            if (!flag2row) {
                flag2row = true
                val layoutManager = GridLayoutManager(
                        this,
                        2,
                        LinearLayoutManager.HORIZONTAL,
                        false
                )
                recyclerViewDock.layoutManager = layoutManager
            } else {
                flag2row = false
                val layoutManager = GridLayoutManager(
                        this,
                        1,
                        LinearLayoutManager.HORIZONTAL,
                        false
                )
                recyclerViewDock.layoutManager = layoutManager
            }
        }
        val rotation = AnimationUtils.loadAnimation(this, R.anim.rotate)
        rotation.fillAfter = true
        all_apps_btn.startAnimation(rotation)
        adapter = DockAdapter(this)
        recyclerViewDock.adapter = adapter
        val layoutManager = GridLayoutManager(this, 1, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewDock.layoutManager = layoutManager
        val itemTouchHelper by lazy {
            // 1. Note that I am specifying all 4 directions.
            //    Specifying START and END also allows
            //    more organic dragging than just specifying UP and DOWN.
            val simpleItemTouchCallback =
                object : ItemTouchHelper.SimpleCallback(
                        UP or
                                DOWN or
                                START or
                                END, 0
                ) {

                    override fun onMove(
                            recyclerView: RecyclerView,
                            viewHolder: RecyclerView.ViewHolder,
                            target: RecyclerView.ViewHolder
                    ): Boolean {
                        Flashbar.Builder(this@MainActivity)
                            .gravity(Flashbar.Gravity.TOP)
                            .title(R.string.app_name)
                            .message("Drag icon left or right to adjust position")
                            .duration(1500)
                            .backgroundColorRes(R.color.default_color)
                            .build().show()
                        // adapter = recyclerView.adapter as DockAdapter
                        val from = viewHolder.adapterPosition
                        val to = target.adapterPosition
                        // 2. Update the backing model. Custom implementation in
                        //    MainRecyclerViewAdapter. You need to implement
                        //    reordering of the backing model inside the method.
                        adapter.moveItem(from, to)
                        // 3. Tell adapter to render the model update.
                        adapter.notifyItemMoved(from, to)
                        return true
                    }

                    override fun onSwiped(
                            viewHolder: RecyclerView.ViewHolder,
                            direction: Int
                    ) {
                        // 4. Code block for horizontal swipe.
                        //    ItemTouchHelper handles horizontal swipe as well, but
                        //    it is not relevant with reordering. Ignoring here.
                    }
                }
            ItemTouchHelper(simpleItemTouchCallback)
        }
        itemTouchHelper.attachToRecyclerView(recyclerViewDock)
    }

    override fun onSendAppObject(appInfo: AppInfo) {
        Flashbar.Builder(this)
            .gravity(Flashbar.Gravity.TOP)
            .message("adding ${appInfo.label} to Chromium launcher dock")
            .duration(2000)
            .backgroundColorRes(R.color.default_color)
            .build().show()

        if (recyclerViewDock != null) {
            recyclerViewDock.smoothScrollToPosition(0)
            adapter.addNewApp(appInfo)
            shakeItems(this, recyclerViewDock)
        }
        GuideView.Builder(this)
                .setTitle(appInfo.label.toString())
                .setContentText(appInfo.label.toString() + " added to dock")
                .setDismissType(DismissType.anywhere) //optional - default DismissType.targetView
                .setTargetView(recyclerViewDock)
                .setContentTextSize(12) //optional
                .setTitleTextSize(14) //optional
                .build()
                .show()
        adapter.notifyDataSetChanged()
    }

    override fun onSentPosition(x: Float, y: Float) {
        Toast.makeText(
                applicationContext,
                "position X : ${x}\nposition Y :${y}",
                Toast.LENGTH_SHORT
        ).show()
    }

    override fun onLaunchApp() {
        minimize()
    }

    override fun minimize() {
        home_viewpager.setCurrentItem(0, true)
    }

    private fun ViewPager2.reduceDragSensitivity() {
        val recyclerViewField = ViewPager2::class.java.getDeclaredField("mRecyclerView")
        recyclerViewField.isAccessible = true
        val recyclerView = recyclerViewField.get(this) as RecyclerView
        val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop")
        touchSlopField.isAccessible = true
        val touchSlop = touchSlopField.get(recyclerView) as Int
        touchSlopField.set(recyclerView, touchSlop * 7)       // "8" was obtained experimentally
    }
}
