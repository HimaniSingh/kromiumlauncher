package com.kromium.launcher

import android.graphics.drawable.Drawable

class AppInfo {
    var label: CharSequence? = null
    var packageName: CharSequence? = null
    var icon: Drawable? = null
}