package com.kromium.launcher

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*


class AppDrawerFragment : Fragment() {


    private lateinit var searchView: androidx.appcompat.widget.SearchView
    private lateinit var adapter: DrawerAdapter
    private lateinit var appsList: ArrayList<AppInfo>

    private lateinit var listener: DrawerAdapter.AppDrawerListener

    // Store instance variables based on arguments passed
    @SuppressLint("QueryPermissionsNeeded")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        //This is where we build our list of app details, using the app
        //object we created to store the label, package name and icon
        val pm: PackageManager = requireContext().packageManager
        appsList = ArrayList<AppInfo>()

        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        val allApps = pm.queryIntentActivities(i, 0)
        allApps.sortWith { emp1, emp2 ->
            emp1.loadLabel(pm).toString()
                .compareTo(emp2.loadLabel(pm).toString(), ignoreCase = true)
        }
        for (ri in allApps) {
            val app = AppInfo()
            app.label = ri.loadLabel(pm)
            app.packageName = ri.activityInfo.packageName
            app.icon = ri.activityInfo.loadIcon(pm)
            appsList.add(app)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_app_drawer, container, false)
        /* val drawerAdapter = DrawerAdapter(activity, {
             context?.let { it1 ->
                 launchApp(it1, it.packageName.toString())
             }
         }, {
             listener.onSendAppObject(it)
         })*/
        val minimizeBtn: Button = rootView.findViewById(R.id.minimizeBtn)
        minimizeBtn.setOnClickListener { listener.minimize() }
        val recyclerView: RecyclerView = rootView.findViewById(R.id.AppDrawerRecyclerView)
        searchView= rootView.findViewById(R.id.app_search)
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })
        recyclerView.setHasFixedSize(true)
        adapter = DrawerAdapter(appsList, context)
        recyclerView.layoutManager = GridLayoutManager(context, 4)
        recyclerView.adapter = adapter
        recyclerView.requestDisallowInterceptTouchEvent(true)
        recyclerView.hasFixedSize()
        return rootView
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DrawerAdapter.AppDrawerListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement AppDrawerListener")
        }
    }

}