package com.kromium.launcher

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.OnLongClickListener
import android.view.View.OnTouchListener
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    private lateinit var inflater: LayoutInflater
    private val lastTouchDownXY = FloatArray(2)
    private lateinit var alertDialog: AlertDialog

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this.inflater = inflater
        val rootview = inflater.inflate(R.layout.fragment_home, container, false)
        val relativeLayout: RelativeLayout = rootview.findViewById(R.id.homeRelativeLayout)
        val micBtn: ImageView = rootview.findViewById(R.id.micBtn)
        val googleBtn: RelativeLayout = rootview.findViewById(R.id.GoogleWidget)
        micBtn.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VOICE_COMMAND).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            } catch (e: Exception) {
            } finally {
            }
        }
        googleBtn.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_WEB_SEARCH).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            } catch (e: Exception) {
            } finally {
            }
        }
        relativeLayout.setOnTouchListener(touchListener);
        relativeLayout.setOnLongClickListener(longClickListener);

        return rootview
    }

    companion object {
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    @SuppressLint("ClickableViewAccessibility")
    var touchListener = OnTouchListener { v, event -> // save the X,Y coordinates
        if (event.actionMasked == MotionEvent.ACTION_DOWN) {
            lastTouchDownXY[0] = event.x
            lastTouchDownXY[1] = event.y
        }

        // let the touch event pass on to whoever needs it
        false
    }
    private var longClickListener = OnLongClickListener { // retrieve the stored coordinates
        val x: Float = lastTouchDownXY[0]
        val y: Float = lastTouchDownXY[1]

        // use the coordinates for whatever
        Log.i("TAG", "onLongClick: x = $x, y = $y")

        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(
            context
        )
        //dialogBuilder.setTitle(getString(R.string.app_name))
        //dialogBuilder.setIcon(R.mipmap.ic_launcher)
        dialogBuilder.setCancelable(true)
        val dialogView: View = inflater.inflate(R.layout.menu_home, null)
        dialogBuilder.setView(dialogView)

        val wallpaperButton = dialogView.findViewById<View>(R.id.wallpaperBtn) as Button
        wallpaperButton.setOnClickListener {
            val intent = Intent(context, Wallpapers::class.java)
            startActivity(intent)
            alertDialog.dismiss()
        }
        val settingsButton = dialogView.findViewById<View>(R.id.launcherSettingBtn) as Button
        settingsButton.setOnClickListener {
            val intent = Intent(context, SettingsActivity::class.java)
            startActivity(intent)
            alertDialog.dismiss()
        }
        val systemSettingsButton =
            dialogView.findViewById<View>(R.id.systemSettingBtn) as Button
        systemSettingsButton.setOnClickListener {
            startActivityForResult(Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            alertDialog.dismiss()
        }
        alertDialog = dialogBuilder.create()
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()
        true
    }
}