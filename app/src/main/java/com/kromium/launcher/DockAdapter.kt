package com.kromium.launcher

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import java.util.*

@SuppressLint("QueryPermissionsNeeded")
class DockAdapter(c: Context) : RecyclerView.Adapter<DockAdapter.ViewHolder>() {
    private val appsList: MutableList<AppInfo?>
    private val context: Context
    fun moveItem(from: Int, to: Int) {
        Collections.swap(appsList, from, to)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        //Here we use the information in the list we created to define the views
        val appLabel = appsList[i]!!.label.toString()
        val appPackage = appsList[i]!!.packageName.toString()
        val appIcon = appsList[i]!!.icon

        //     TextView textView = viewHolder.textView;
        //     textView.setText(appLabel);
        val imageView = viewHolder.img
        imageView.setImageDrawable(appIcon)
    }

    override fun getItemCount(): Int {

        //This method needs to be overridden so that Androids knows how many items
        //will be making it into the list
        return appsList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //This is what adds the code we've written in here to our target view
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.row_dock, parent, false)
        return ViewHolder(view)
    }

    fun addNewApp(appInfo: AppInfo?) {
        appsList.add(0, appInfo)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        // public TextView textView;
        var img: ImageView
        override fun onClick(v: View) {
            val pos = adapterPosition
            val context = v.context
            val launchIntent =
                context.packageManager.getLaunchIntentForPackage(appsList[pos]!!.packageName.toString())
            context.startActivity(launchIntent)
        }

        //This is the subclass ViewHolder which simply
        //'holds the views' for us to show on each row
        init {
            //Finds the views from our row.xml
            //  textView = itemView.findViewById(R.id.tv_label);
            img = itemView.findViewById(R.id.iv_icon)
            itemView.setOnClickListener(this)
        }
    }

    init {

        //This is where we build our list of app details, using the app
        //object we created to store the label, package name and icon
        val pm = c.packageManager
        appsList = ArrayList()
        context = c
        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)
        val allApps = pm.queryIntentActivities(i, 0)
        for (ri in allApps) {
            val app = AppInfo()
            app.label = ri.loadLabel(pm)
            app.packageName = ri.activityInfo.packageName
            app.icon = ri.activityInfo.loadIcon(pm)
            if (app.packageName.toString().contains("com.android.") || app.packageName.toString()
                    .contains("whatsapp") || app.packageName.toString()
                    .contains("instagram") || app.packageName.toString().contains("google")
            ) {
                appsList.add(app)
            }
        }
    }
}