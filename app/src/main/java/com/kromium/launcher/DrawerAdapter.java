package com.kromium.launcher;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;
import java.util.List;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder> implements Filterable {
    private List<AppInfo> exampleList;
    private List<AppInfo> exampleListFull;
    private Context context;
    private AppDrawerListener listener;
    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<AppInfo> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(exampleListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (AppInfo item : exampleListFull) {
                    if (item.getLabel().toString().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            exampleList.clear();
            exampleList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    DrawerAdapter(List<AppInfo> exampleList, Context context) {
        this.exampleList = exampleList;
        exampleListFull = new ArrayList<>(exampleList);
        this.context = context;
        this.listener = (AppDrawerListener) context;
    }

    @Override
    public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row,
                parent, false);
        return new DrawerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DrawerViewHolder holder, int position) {
        AppInfo currentItem = exampleList.get(position);
        holder.icon.setImageDrawable(currentItem.getIcon());
        holder.label.setText(currentItem.getLabel());
        holder.itemView.setOnClickListener(view -> {
            launchApp(context, exampleList.get(position).getPackageName().toString());
        });
        holder.itemView.setOnLongClickListener(view -> {
            new MaterialAlertDialogBuilder(context)
                    .setTitle("Choose option for " + exampleList.get(position).getLabel())
                    .setIcon(exampleList.get(position).getIcon())
                    .setMessage("Note : add to dock adds app to bottom of home screen")
                    .setPositiveButton("add to dock", (dialogInterface, i) -> {
                        listener.onSendAppObject(exampleList.get(position));
                        listener.minimize();
                    })
                    .setNegativeButton("uninstall", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_DELETE);
                            intent.setData(Uri.parse("package:" + exampleList.get(position).getPackageName().toString()));
                            context.startActivity(intent);
                        }
                    }).setNeutralButton("Info", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        //Open the specific App Info page:
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + exampleList.get(position).getPackageName()));
                        context.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //e.printStackTrace();
                        //Open the generic Apps page:
                        Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                        context.startActivity(intent);
                    }
                }
            })
                    .show();
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return exampleList.size();
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private void launchApp(Context context, String packageName) {
        // Get an instance of PackageManager
        PackageManager pm = context.getPackageManager();

        // Initialize a new Intent
        Intent intent = pm.getLaunchIntentForPackage(packageName);

        // Add category to intent
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        // If intent is not null then launch the app
        if (intent != null) {
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Intent null.", Toast.LENGTH_SHORT).show();
        }
        listener.onLaunchApp();
    }

    interface AppDrawerListener {
        void onSendAppObject(AppInfo appInfo);
        void onLaunchApp();
        void onSentPosition(Float x, Float y);

        void minimize();
    }

    class DrawerViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView label;

        DrawerViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.iv_icon);
            label = itemView.findViewById(R.id.tv_label);
        }
    }

}
