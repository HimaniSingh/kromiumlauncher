package com.kromium.launcher

import android.app.WallpaperManager
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.io.IOException

class Wallpapers : AppCompatActivity() {
    private lateinit var wallpaperbg: RelativeLayout
    private var currentDrawable: Drawable? = null
    private var bottomSheetBehavior: BottomSheetBehavior<GridView>? = null

    // --Commented out by Inspection (27-May-19 11:45 PM):boolean isLoaded = false;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallpapers)
        val imageId = intArrayOf(
            R.drawable.small1,
            R.drawable.small1,
            R.drawable.small1,
            R.drawable.small1,
            R.drawable.small1,
            R.drawable.small1
        )
        val wallpaperManager = WallpaperManager.getInstance(applicationContext)
        val wallpaper = wallpaperManager.drawable
        findViewById<View>(R.id.wallpapersbg).background = wallpaper
        val adapter = ImageAdapter(this@Wallpapers, imageId)
        wallpaperbg = findViewById(R.id.wallpapersbg)
        val wallpaperBtn = findViewById<Button>(R.id.wallpaperbtn)
        val wallpaperGridView = findViewById<GridView>(R.id.wallpaper_grid)
        val moreWallpaperBtn = findViewById<TextView>(R.id.more_wallpaper_btn)
        wallpaperGridView.adapter = adapter
        bottomSheetBehavior = BottomSheetBehavior.from(wallpaperGridView)
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior!!.skipCollapsed = false
        wallpaperGridView.onItemClickListener =
            AdapterView.OnItemClickListener { parent: AdapterView<*>?, view: View, position: Int, id: Long ->
                if (adapter.isLoaded) {
                    val viewGroup = view as ViewGroup
                    val imageView = viewGroup.getChildAt(0) as ImageView
                    val drawable = imageView.drawable
                    currentDrawable = drawable
                    wallpaperbg.setBackgroundDrawable(drawable)
                    bottomSheetBehavior!!.setState(BottomSheetBehavior.STATE_COLLAPSED)
                } else {
                    Toast.makeText(
                        applicationContext, """
     Internet Error
     Connect to internet and try again.
     ${adapter.errorMessage}
     """.trimIndent(), Toast.LENGTH_SHORT
                    ).show()
                }
            }
        wallpaperbg.setOnClickListener(View.OnClickListener { v: View? ->
            bottomSheetBehavior!!.setState(
                BottomSheetBehavior.STATE_EXPANDED
            )
        })
        moreWallpaperBtn.setOnClickListener { v: View? ->
            val intent = Intent(Intent.ACTION_SET_WALLPAPER)
            startActivity(Intent.createChooser(intent, "Select Wallpaper"))
        }
        wallpaperBtn.setOnClickListener { v: View? ->
            val wallpaperManager1 = WallpaperManager.getInstance(this@Wallpapers)
            if (currentDrawable == null) {
                Toast.makeText(
                    applicationContext,
                    "Error Setting wallpaper(select wallpaper first)",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val bitmap = (currentDrawable as BitmapDrawable).bitmap
                AsyncTask.execute {
                    try {
                        wallpaperManager1.setBitmap(bitmap)
                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(
                            applicationContext,
                            "Error !" + e.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                /* runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
               */Toast.makeText(applicationContext, "set wallpaper success !", Toast.LENGTH_SHORT)
                    .show()
                onBackPressed()
            }
        }
    }
}